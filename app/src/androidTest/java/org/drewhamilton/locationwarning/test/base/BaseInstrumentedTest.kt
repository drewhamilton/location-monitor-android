package org.drewhamilton.locationwarning.test.base

import android.content.Context
import android.support.annotation.CallSuper
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Before
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
abstract class BaseInstrumentedTest {

  protected lateinit var applicationContext: Context
    private set

  @Before
  @CallSuper
  open fun setUp() {
    applicationContext = InstrumentationRegistry.getTargetContext()
  }
}
