package org.drewhamilton.locationwarning.test.base

import android.app.Activity
import android.content.Intent
import android.support.annotation.CallSuper
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.matcher.ViewMatchers.isRoot
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.drewhamilton.locationwarning.inject.AppComponentHolder
import org.drewhamilton.locationwarning.test.espresso.idleOnMainForAtLeast
import org.drewhamilton.locationwarning.test.inject.DaggerTestAppComponent
import org.drewhamilton.locationwarning.test.inject.MockAndroidModule
import org.drewhamilton.locationwarning.test.inject.MockContextModule
import org.drewhamilton.locationwarning.test.inject.TestAppComponent
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import kotlin.reflect.KClass

@RunWith(AndroidJUnit4::class)
abstract class BaseUiTest<A: Activity> constructor(activityClass: KClass<A>) {

  @Rule
  @JvmField
  val activityRule: ActivityTestRule<A> = ActivityTestRule(activityClass.java, true, false)

  protected lateinit var appComponent: TestAppComponent
    private set

  @Before
  @CallSuper
  open fun setUp() {
    appComponent = buildTestAppComponent()
    AppComponentHolder.setAppComponent(appComponent)
  }

  protected fun launchActivity() {
    activityRule.launchActivity(Intent())
  }

  protected fun launchActivity(intent: Intent) {
    activityRule.launchActivity(intent)
  }

  protected fun getActivity(): A {
    val activity: A? = activityRule.activity
    return if (activity == null) {
      fail("Called getActivity() before launching Activity")

      @Suppress("UNREACHABLE_CODE") // Need this return for compilation
      null!!
    } else {
      activity
    }
  }

  protected fun runOnUiThread(function: () -> Unit) {
    activityRule.runOnUiThread { function.invoke() }
  }

  protected fun waitForIdleSync() {
    InstrumentationRegistry.getInstrumentation().waitForIdleSync()
  }

  protected fun waitOnMain(milliseconds: Long) {
    Espresso.onView(isRoot())
        .perform(idleOnMainForAtLeast(milliseconds))
  }

  private fun buildTestAppComponent(): TestAppComponent {
    return DaggerTestAppComponent.builder()
        .mockContextModule(MockContextModule())
        .mockAndroidModule(MockAndroidModule())
        .build()
  }
}
