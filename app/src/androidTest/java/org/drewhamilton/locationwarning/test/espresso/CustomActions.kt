package org.drewhamilton.locationwarning.test.espresso

import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.view.View
import org.hamcrest.Matcher
import org.hamcrest.Matchers.any

fun idleOnMainForAtLeast(milliseconds: Long): ViewAction {
  return object : ViewAction {
    override fun getConstraints(): Matcher<View> {
      return any(View::class.java)
    }

    override fun getDescription(): String {
      return "idle on main thread for $milliseconds milliseconds"
    }

    override fun perform(uiController: UiController, view: View) {
      uiController.loopMainThreadForAtLeast(milliseconds)
    }
  }
}
