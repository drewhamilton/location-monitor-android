package org.drewhamilton.locationwarning.test.inject

import android.app.NotificationManager
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MockAndroidModule {

  @Provides
  @Singleton
  fun provideNotificationManager(): NotificationManager = mock()
}
