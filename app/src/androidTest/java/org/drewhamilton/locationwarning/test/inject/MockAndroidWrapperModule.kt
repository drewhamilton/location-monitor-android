package org.drewhamilton.locationwarning.test.inject

import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import org.drewhamilton.locationwarning.wrapper.android.AppCompatDelegate
import javax.inject.Singleton

@Module
class MockAndroidWrapperModule {

  @Provides
  @Singleton
  fun provideAppCompatDelegate(): AppCompatDelegate = mock()
}
