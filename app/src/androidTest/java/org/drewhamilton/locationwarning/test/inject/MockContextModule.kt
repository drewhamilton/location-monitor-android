package org.drewhamilton.locationwarning.test.inject

import android.content.Context
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MockContextModule {

  @Provides
  @Singleton
  fun provideApplicationContext(): Context = mock()
}
