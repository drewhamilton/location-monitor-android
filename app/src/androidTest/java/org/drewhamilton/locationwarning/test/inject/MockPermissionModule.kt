package org.drewhamilton.locationwarning.test.inject

import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import org.drewhamilton.locationwarning.permission.PermissionManager
import javax.inject.Singleton

@Module
class MockPermissionModule {

  @Provides
  @Singleton
  fun providePermissionManager(): PermissionManager = mock()
}
