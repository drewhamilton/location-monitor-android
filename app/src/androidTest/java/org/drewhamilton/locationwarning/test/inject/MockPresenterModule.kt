package org.drewhamilton.locationwarning.test.inject

import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import org.drewhamilton.locationwarning.ui.home.HomePresenter
import javax.inject.Singleton

@Module
class MockPresenterModule {

  @Provides
  @Singleton
  fun provideHomePresenter(): HomePresenter = mock()
}
