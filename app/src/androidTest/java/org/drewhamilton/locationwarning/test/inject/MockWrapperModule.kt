package org.drewhamilton.locationwarning.test.inject

import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import org.drewhamilton.locationwarning.wrapper.ExceptionThrower
import javax.inject.Singleton

@Module
class MockWrapperModule {

  @Provides
  @Singleton
  fun provideExceptionThrower(): ExceptionThrower = mock()
}
