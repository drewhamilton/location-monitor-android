package org.drewhamilton.locationwarning.test.inject

import dagger.Component
import org.drewhamilton.locationwarning.inject.AppComponent
import org.drewhamilton.locationwarning.ui.base.BaseActivityTest
import org.drewhamilton.locationwarning.ui.base.BaseActivityWithoutViewImplTest
import org.drewhamilton.locationwarning.ui.home.HomeActivityTest
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    MockAndroidModule::class,
    MockAndroidWrapperModule::class,
    MockContextModule::class,
    MockPermissionModule::class,
    MockPresenterModule::class,
    MockWrapperModule::class
))
interface TestAppComponent: AppComponent {

  //region Injections
  fun inject(baseActivityTest: BaseActivityTest)

  fun inject(baseActivityWithoutViewImplTest: BaseActivityWithoutViewImplTest)

  fun inject(homeActivityTest: HomeActivityTest)
  //endregion
}
