package org.drewhamilton.locationwarning.test.ui

import org.drewhamilton.locationwarning.ui.base.BaseView

class BaseActivityBaseViewImpl: BaseActivityImpl(), BaseView
