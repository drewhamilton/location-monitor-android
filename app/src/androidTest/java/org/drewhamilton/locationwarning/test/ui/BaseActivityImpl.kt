package org.drewhamilton.locationwarning.test.ui

import android.os.Bundle
import com.nhaarman.mockito_kotlin.mock
import org.drewhamilton.locationwarning.R
import org.drewhamilton.locationwarning.ui.base.BaseActivity
import org.drewhamilton.locationwarning.ui.base.BasePresenter
import org.drewhamilton.locationwarning.ui.base.BaseView

open class BaseActivityImpl : BaseActivity<BaseView, BasePresenter<BaseView>>() {

  var wasOnPreAttachPresenterCalled: Boolean = false
    private set

  override fun getLayout() = R.layout.test_layout

  override fun newPresenter(): BasePresenter<BaseView> = mock()

  override fun onCreate(savedInstanceState: Bundle?) {
    wasOnPreAttachPresenterCalled = false
    super.onCreate(savedInstanceState)
  }

  override fun onPreAttachPresenter() {
    wasOnPreAttachPresenterCalled = true
  }

  fun presenter() = presenter
}
