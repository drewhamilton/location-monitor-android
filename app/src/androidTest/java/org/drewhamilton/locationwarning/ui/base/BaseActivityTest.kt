package org.drewhamilton.locationwarning.ui.base

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.amshove.kluent.shouldBe
import org.drewhamilton.locationwarning.R
import org.drewhamilton.locationwarning.permission.PermissionManager
import org.drewhamilton.locationwarning.test.base.BaseUiTest
import org.drewhamilton.locationwarning.test.ui.BaseActivityBaseViewImpl
import org.drewhamilton.locationwarning.wrapper.android.AppCompatDelegate
import org.junit.Test
import org.mockito.Mockito
import javax.inject.Inject

class BaseActivityTest: BaseUiTest<BaseActivityBaseViewImpl>(BaseActivityBaseViewImpl::class) {

  private companion object {
    const val NIGHT_MODE = android.support.v7.app.AppCompatDelegate.MODE_NIGHT_AUTO
  }

  @Inject lateinit var mockAppCompatDelegate: AppCompatDelegate
  @Inject lateinit var mockPermissionManager: PermissionManager

  override fun setUp() {
    super.setUp()
    appComponent.inject(this)
  }

  @Test
  fun onCreate_layoutIsSet() {
    launchActivity()

    onView(withId(R.id.test_layout_id)).check(matches(isDisplayed()))
  }

  @Test
  fun onCreate_hasLocationPermission_doesNotAskAgain() {
    whenever(mockPermissionManager.hasLocationPermission(any())).thenReturn(true)

    launchActivity()

    verify(mockPermissionManager).hasLocationPermission(getActivity())
    verify(mockPermissionManager, never()).requestLocationPermission(any())
  }

  @Test
  fun onCreate_doesNotHaveLocationPermission_asksForLocationPermission() {
    whenever(mockPermissionManager.hasLocationPermission(any())).thenReturn(false)

    launchActivity()

    verify(mockPermissionManager).hasLocationPermission(getActivity())
    verify(mockPermissionManager).requestLocationPermission(getActivity())
  }

  @Test
  fun onCreate_hasCorrectNightMode_doesNotSetNightModeAgain() {
    whenever(mockAppCompatDelegate.getDefaultNightMode()).thenReturn(NIGHT_MODE)

    launchActivity()

    verify(mockAppCompatDelegate).getDefaultNightMode()
    verify(mockAppCompatDelegate, never()).setDefaultNightMode(any())
  }

  @Test
  fun onCreate_hasIncorrectNightMode_setsCorrectNightMode() {
    whenever(mockAppCompatDelegate.getDefaultNightMode()).thenReturn(android.support.v7.app.AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

    launchActivity()

    verify(mockAppCompatDelegate).getDefaultNightMode()
    verify(mockAppCompatDelegate).setDefaultNightMode(NIGHT_MODE)
  }

  @Test
  fun onCreate_attachesToPresenter() {
    launchActivity()

    getActivity().wasOnPreAttachPresenterCalled shouldBe true
    verify(getActivity().presenter()).attachView(getActivity())
  }

  @Test
  fun onDestroy_detachesFromPresenter() {
    launchActivity()
    val presenter = getActivity().presenter()
    getActivity().finish()
    // Can't use waitOnMain() because Activity has finished:
    sleep(400)

    verify(presenter).detachView()
  }

  @Test
  fun onRequestPermissionsResult_locationPermission_checksPermissionStatus() {
    launchActivity()
    Mockito.reset(mockPermissionManager)
    getActivity().onRequestPermissionsResult(PermissionManager.REQUEST_CODE_LOCATION, emptyArray(), IntArray(0))

    verify(mockPermissionManager).hasLocationPermission(getActivity())
  }

  @Test
  fun onRequestPermissionsResult_otherPermission_doesNotCheckPermissionStatus() {
    launchActivity()
    Mockito.reset(mockPermissionManager)
    getActivity().onRequestPermissionsResult(1, emptyArray(), IntArray(0))

    verify(mockPermissionManager, never()).hasLocationPermission(any())
  }

  // Use this very sparingly: prefer waitOnMain() when at all possible
  private fun sleep(milliseconds: Long) {
    Thread.sleep(milliseconds)
  }
}
