package org.drewhamilton.locationwarning.ui.base

import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import org.amshove.kluent.shouldBe
import org.drewhamilton.locationwarning.test.base.BaseUiTest
import org.drewhamilton.locationwarning.test.ui.BaseActivityImpl
import org.drewhamilton.locationwarning.wrapper.ExceptionThrower
import org.junit.Test
import javax.inject.Inject

class BaseActivityWithoutViewImplTest : BaseUiTest<BaseActivityImpl>(BaseActivityImpl::class) {

  @Inject lateinit var exceptionThrower: ExceptionThrower

  override fun setUp() {
    super.setUp()
    appComponent.inject(this)
  }

  @Test
  fun onCreate_throwsException() {
    launchActivity()

    val exceptionCaptor = argumentCaptor<InvalidViewImplementationException>()
    verify(exceptionThrower) throwException exceptionCaptor.capture()
    exceptionCaptor.allValues.size shouldBe 1

    verifyNoMoreInteractions(getActivity().presenter())
  }
}
