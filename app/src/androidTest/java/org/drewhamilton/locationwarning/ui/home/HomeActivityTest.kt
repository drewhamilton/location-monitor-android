package org.drewhamilton.locationwarning.ui.home

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import com.nhaarman.mockito_kotlin.verify
import org.drewhamilton.locationwarning.R
import org.drewhamilton.locationwarning.test.base.BaseUiTest
import org.hamcrest.Matchers.not
import org.junit.Test
import javax.inject.Inject

class HomeActivityTest: BaseUiTest<HomeActivity>(HomeActivity::class) {

  private companion object {
    const val TEST_MESSAGE = "Test message"
  }

  @Inject lateinit var mockHomePresenter: HomePresenter

  override fun setUp() {
    super.setUp()
    appComponent.inject(this)
  }

  @Test
  fun attachesToPresenter() {
    launchActivity()
    verify(mockHomePresenter).attachView(getActivity())
  }

  @Test
  fun displayActiveNotificationUi() {
    launchActivity()
    runOnUiThread { getActivity().displayActiveNotificationUi() }

    onView(withText(R.string.hide_notification))
        .check(matches(isDisplayed()))
        .check(matches(isEnabled()))
    onView(withText(R.string.show_notification))
        .check(matches(isDisplayed()))
        .check(matches(not(isEnabled())))

    onView(withId(R.id.background_home))
        .check(matches(withAlpha(1f)))
  }

  @Test
  fun displayInactiveNotificationUi() {
    launchActivity()
    runOnUiThread { getActivity().displayInactiveNotificationUi() }

    onView(withText(R.string.hide_notification))
        .check(matches(isDisplayed()))
        .check(matches(not(isEnabled())))
    onView(withText(R.string.show_notification))
        .check(matches(isDisplayed()))
        .check(matches(isEnabled()))

    onView(withId(R.id.background_home))
        .check(matches(withAlpha(0f)))
  }

  @Test
  fun displayNeutralUi() {
    launchActivity()
    runOnUiThread { getActivity().displayNeutralUi() }

    onView(withText(R.string.hide_notification))
        .check(matches(isDisplayed()))
        .check(matches(isEnabled()))
    onView(withText(R.string.show_notification))
        .check(matches(isDisplayed()))
        .check(matches(isEnabled()))

    onView(withId(R.id.background_home))
        .check(matches(withAlpha(0f)))
  }

  @Test
  fun displayDraftText() {
    val editedTextSuffix = " edited"

    launchActivity()
    runOnUiThread { getActivity().displayDraftText(TEST_MESSAGE) }

    onView(withText(TEST_MESSAGE))
        .check(matches(isDisplayed()))
        .perform(typeText(editedTextSuffix))
        .check(doesNotExist())

    val editedText = TEST_MESSAGE + editedTextSuffix
    onView(withText(editedText))
        .check(matches(isDisplayed()))
    verify(mockHomePresenter).updateDraftText(editedText)
  }

  @Test
  fun setCanPostNotification_false_showNotificationButtonIsDisabled() {
    launchActivity()
    runOnUiThread {
      getActivity().displayNeutralUi()
      getActivity().setCanPostNotification(false)
    }

    onView(withText(R.string.show_notification))
        .check(matches(not(isEnabled())))
  }

  @Test
  fun setCanPostNotification_true_showNotificationButtonIsEnabled() {
    launchActivity()
    runOnUiThread {
      getActivity().displayNeutralUi()
      getActivity().setCanPostNotification(true)
    }

    onView(withText(R.string.show_notification))
        .check(matches(isEnabled()))
  }

  @Test
  fun clickHideNotification_hidesNotification() {
    launchActivity()
    runOnUiThread { getActivity().displayNeutralUi() }

    onView(withText(R.string.hide_notification))
        .check(matches(isDisplayed()))
        .check(matches(isEnabled()))
        .perform(click())

    verify(mockHomePresenter).hideNotification()
  }

  @Test
  fun clickShowNotification_showsNotificationWithCorrectMessage() {
    launchActivity()
    runOnUiThread {
      getActivity().displayDraftText(TEST_MESSAGE)
      getActivity().displayNeutralUi()
    }

    onView(withText(R.string.show_notification))
        .check(matches(isDisplayed()))
        .check(matches(isEnabled()))
        .perform(click())

    verify(mockHomePresenter).showNotification(TEST_MESSAGE)
  }
}
