package org.drewhamilton.locationwarning.wrapper.android

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.amshove.kluent.mock
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldEqual
import org.drewhamilton.locationwarning.test.base.BaseInstrumentedTest
import org.junit.Test

class ActivityCompatTest: BaseInstrumentedTest() {

  private lateinit var activityCompat: ActivityCompat

  override fun setUp() {
    super.setUp()
    activityCompat = ActivityCompat()
  }

  @Test
  fun checkSelfPermission_checkContextPermission() {
    val mockContext: Context = mock()
    val fakePermission = "Fake permission"
    val fakeReturnValue = 345

    whenever(mockContext.checkPermission(eq(fakePermission), any(), any())).thenReturn(fakeReturnValue)

    val result = activityCompat.checkSelfPermission(mockContext, fakePermission)
    result shouldEqual fakeReturnValue
  }

  @Test
  fun requestPermissions() {
    val mockPackageManager: PackageManager = mock()

    val mockActivity: Activity = mock()
    whenever(mockActivity.packageManager).thenReturn(mockPackageManager)

    val fakePermission = "Fake permission"
    val fakePermissionsArray = arrayOf(fakePermission)
    val fakeRequestCode = 83

    activityCompat.requestPermissions(mockActivity, fakePermissionsArray, fakeRequestCode)

    // This is a pretty lame assertion but I can't find anything else that can be asserted.
    verify(mockActivity).packageManager
  }

  @Test
  fun shouldShowRequestPermissionRationale_activityReturnsFalse_returnsFalse() {
    testShouldShowRequestPermissionRationale(false)
  }

  @Test
  fun shouldShowRequestPermissionRationale_activityReturnsTrue_returnsTrue() {
    testShouldShowRequestPermissionRationale(true)
  }

  private fun testShouldShowRequestPermissionRationale(activityResult: Boolean) {
    val mockActivity: Activity = mock()
    val fakePermission = "Fake permission"
    whenever(mockActivity.shouldShowRequestPermissionRationale(fakePermission)).thenReturn(activityResult)

    val result = activityCompat.shouldShowRequestPermissionRationale(mockActivity, fakePermission)

    result shouldBe activityResult
  }
}
