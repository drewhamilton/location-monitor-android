package org.drewhamilton.locationwarning.wrapper.android.factory

import org.amshove.kluent.shouldBeEqualTo
import org.drewhamilton.locationwarning.test.base.BaseInstrumentedTest
import org.drewhamilton.locationwarning.test.ui.BaseActivityBaseViewImpl
import org.junit.Test

class IntentFactoryTest: BaseInstrumentedTest() {

  private lateinit var intentFactory: IntentFactory

  override fun setUp() {
    super.setUp()
    intentFactory = IntentFactory()
  }

  @Test
  fun intent_usesContextAndActivityClass() {
    val activityClass = BaseActivityBaseViewImpl::class.java
    val result = intentFactory.intent(applicationContext, activityClass)

    result.component.packageName shouldBeEqualTo applicationContext.packageName
    result.component.className shouldBeEqualTo activityClass.name
  }

  @Test
  fun createActivityPendingIntent_passesParametersToGetActivity() {
    val requestCode = 834
    val intent = intentFactory.intent(applicationContext, BaseActivityBaseViewImpl::class.java)
    val flags = 98843
    val result = intentFactory.createActivityPendingIntent(applicationContext, requestCode, intent, flags)

    // TODO Can this be tested more thoroughly?
    result.creatorPackage shouldBeEqualTo applicationContext.packageName
  }
}
