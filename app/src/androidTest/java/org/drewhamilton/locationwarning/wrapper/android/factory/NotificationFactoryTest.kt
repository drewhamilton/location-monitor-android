package org.drewhamilton.locationwarning.wrapper.android.factory

import android.app.NotificationManager
import android.support.v4.app.NotificationCompat
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldEqual
import org.drewhamilton.locationwarning.test.base.BaseInstrumentedTest
import org.junit.Test

class NotificationFactoryTest: BaseInstrumentedTest() {

  private lateinit var notificationFactory: NotificationFactory

  override fun setUp() {
    super.setUp()
    notificationFactory = NotificationFactory(applicationContext)
  }

  @Test
  fun notificationChannel_returnsChannelWithIdNameAndImportance() {
    val id = "ID"
    val name = "Name"
    val importance = 87

    val result = notificationFactory.notificationChannel(id, name, importance)

    result.id shouldBeEqualTo id
    result.name shouldEqual name
    result.importance shouldEqual importance
  }

  @Test
  fun notificationBuilder_withChannel_returnsBuilderWithChannel() {
    val id = "Channel ID"
    val name = "Channel name"
    val importance = NotificationManager.IMPORTANCE_DEFAULT
    val fakeChannel = notificationFactory.notificationChannel(id, name, importance)

    val result = notificationFactory.notificationBuilder(fakeChannel)

    val builtResult = result.build()
    builtResult.channelId shouldBeEqualTo id
  }

  @Test
  fun notificationBuilder_withoutChannel_returnsBasicBuilder() {
    val result = notificationFactory.notificationBuilder()

    val builtResult = result.build()
    builtResult.channelId shouldBe null
  }

  @Test
  fun bigTextStyle_returnsPlainBigTextStyle() {
    val result = notificationFactory.bigTextStyle()
    result.javaClass shouldBe NotificationCompat.BigTextStyle::class.java
  }
}
