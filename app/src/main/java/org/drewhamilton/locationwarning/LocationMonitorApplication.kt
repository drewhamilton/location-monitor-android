package org.drewhamilton.locationwarning

import android.app.Application
import org.drewhamilton.locationwarning.inject.*
import timber.log.Timber

class LocationMonitorApplication : Application() {

  override fun onCreate() {
    super.onCreate()

    if (BuildConfig.DEBUG) {
      Timber.plant(Timber.DebugTree())
    }

    AppComponentHolder.setAppComponent(buildAppComponent())
  }

  private fun buildAppComponent(): AppComponent {
    return DaggerAppComponent.builder()
        .contextModule(ContextModule(applicationContext))
        .androidModule(AndroidModule())
        .build()
  }
}
