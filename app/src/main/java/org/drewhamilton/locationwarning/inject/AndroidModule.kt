package org.drewhamilton.locationwarning.inject

import android.app.NotificationManager
import android.content.Context
import dagger.Module
import dagger.Provides

@Module(includes = arrayOf(ContextModule::class))
class AndroidModule {

  @Provides
  fun provideNotificationManager(applicationContext: Context): NotificationManager {
    return applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
  }
}
