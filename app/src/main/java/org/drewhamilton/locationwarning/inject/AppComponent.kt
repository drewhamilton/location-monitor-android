package org.drewhamilton.locationwarning.inject

import dagger.Component
import org.drewhamilton.locationwarning.ui.base.BaseActivity
import org.drewhamilton.locationwarning.ui.home.HomePresenter
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    AndroidModule::class,
    ContextModule::class
))
interface AppComponent {

  //region Getters
  fun homePresenter(): HomePresenter
  //endregion

  //region Injections
  fun inject(injectedMembers: BaseActivity.InjectedMembers)
  //endregion
}
