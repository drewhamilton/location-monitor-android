package org.drewhamilton.locationwarning.inject

class AppComponentHolder private constructor() {

  private var appComponent: AppComponent? = null

  companion object {
    private val INSTANCE = AppComponentHolder()

    fun getAppComponent(): AppComponent {
      return INSTANCE.appComponent!!
    }

    fun setAppComponent(appComponent: AppComponent) {
      INSTANCE.appComponent = appComponent
    }
  }
}
