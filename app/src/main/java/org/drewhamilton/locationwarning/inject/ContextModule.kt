package org.drewhamilton.locationwarning.inject

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ContextModule constructor(private val applicationContext: Context) {

  @Provides
  fun provideApplicationContext(): Context {
    return applicationContext
  }
}
