package org.drewhamilton.locationwarning.notification

import android.app.Notification
import android.app.NotificationChannel
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.service.notification.StatusBarNotification
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import org.drewhamilton.locationwarning.R
import org.drewhamilton.locationwarning.persist.PreferenceRetriever
import org.drewhamilton.locationwarning.ui.home.HomeActivity
import org.drewhamilton.locationwarning.wrapper.android.AndroidVersion
import org.drewhamilton.locationwarning.wrapper.android.NotificationManager
import org.drewhamilton.locationwarning.wrapper.android.factory.IntentFactory
import org.drewhamilton.locationwarning.wrapper.android.factory.NotificationFactory
import timber.log.Timber
import javax.inject.Inject

class PrimaryNotificationController @Inject constructor(
    private val applicationContext: Context,
    private val androidVersion: AndroidVersion,
    private val notificationFactory: NotificationFactory,
    private val notificationManager: NotificationManager,
    private val preferenceRetriever: PreferenceRetriever,
    private val intentFactory: IntentFactory) {

  companion object {
    private const val NOTIFICATION_ID = 1
    private const val NOTIFICATION_DURATION_MS = 1000
  }

  fun postPrimaryNotification() {
    val builder: NotificationCompat.Builder = if (androidVersion.isAtLeastOreo()) {
      val notificationChannel = createNotificationChannel()
      notificationManager.createNotificationChannel(notificationChannel)
      Timber.v("Created notification channel '%s'", notificationChannel.name)

      notificationFactory.notificationBuilder(notificationChannel)
    } else {
      notificationFactory.notificationBuilder()
    }

    val notification: Notification = buildNotification(builder)
    notificationManager.notify(NOTIFICATION_ID, notification)
  }

  fun hidePrimaryNotification() {
    notificationManager.cancel(NOTIFICATION_ID)
  }

  fun canDetermineActiveNotifications() = androidVersion.isAtLeastMarshmallow()

  @RequiresApi(Build.VERSION_CODES.M)
  fun isPrimaryNotificationShowing(): Boolean {
    val activeNotifications: Array<out StatusBarNotification> = notificationManager.activeNotifications()
    for (notification in activeNotifications) {
      if (NOTIFICATION_ID == notification.id) {
        return true
      }
    }

    return false
  }

  @RequiresApi(Build.VERSION_CODES.O)
  private fun createNotificationChannel(): NotificationChannel {
    val notificationChannel = notificationFactory.notificationChannel(applicationContext.getString(R.string.default_notification_channel_id),
        applicationContext.getString(R.string.default_notification_channel_name), android.app.NotificationManager.IMPORTANCE_HIGH)
    notificationChannel.description = applicationContext.getString(R.string.default_notification_channel_description)
    notificationChannel.lightColor = getNotificationLightColor()
    notificationChannel.enableLights(true)

    return notificationChannel
  }

  private fun buildNotification(builder: NotificationCompat.Builder): Notification {
    val flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
    val homeIntent = intentFactory.intent(applicationContext, HomeActivity::class.java)
    homeIntent.flags = flags
    val pendingIntent = intentFactory.createActivityPendingIntent(applicationContext, 0, homeIntent, flags)

    val description = preferenceRetriever.getPrimaryNotificationMessage()
    val style = notificationFactory.bigTextStyle()
    style.bigText(description)

    return with(builder) {
      setContentTitle(applicationContext.getString(R.string.warning))
      setContentText(description)
      setSmallIcon(R.drawable.ic_warning_white_24dp)
      setContentIntent(pendingIntent)
      setStyle(style)
      setOngoing(true)
      color = getNotificationColor()
      setLights(getNotificationLightColor(), NOTIFICATION_DURATION_MS, NOTIFICATION_DURATION_MS)
      setSound(null)
      build()
    }
  }

  private fun getNotificationColor(): Int {
    return ContextCompat.getColor(applicationContext, R.color.primaryDark)
  }

  // LED light needs specific value of red to display correctly
  private fun getNotificationLightColor(): Int {
    return Color.RED
  }
}
