package org.drewhamilton.locationwarning.permission

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import org.drewhamilton.locationwarning.wrapper.android.ActivityCompat
import timber.log.Timber
import javax.inject.Inject

open class PermissionManager @Inject constructor(private var activityCompat: ActivityCompat) {

  companion object {
    val REQUEST_CODE_LOCATION = 10

    private val PERMISSION_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
  }

  fun hasLocationPermission(context: Context): Boolean {
    return hasPermission(context, PERMISSION_LOCATION)
  }

  fun shouldDisplayLocationPermissionRationale(activity: Activity): Boolean {
    return activityCompat.shouldShowRequestPermissionRationale(activity, PERMISSION_LOCATION)
  }

  fun requestLocationPermission(activity: Activity) {
    Timber.v("Requesting location permission")
    requestPermission(activity, PERMISSION_LOCATION, REQUEST_CODE_LOCATION)
  }

  private fun hasPermission(context: Context, permission: String): Boolean {
    return activityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
  }

  private fun requestPermission(activity: Activity, permission: String, requestCode: Int) {
    activityCompat.requestPermissions(activity, Array(1, { permission }), requestCode)
  }
}
