package org.drewhamilton.locationwarning.persist

internal object Keys {

  const val SHARED_PREFERENCES_NAME = "org.drewhamilton.locationwarning\$preferences_primary"

  const val PRIMARY_NOTIFICATION_TEXT = "preferences_primary\$primaryNotificationText"
}
