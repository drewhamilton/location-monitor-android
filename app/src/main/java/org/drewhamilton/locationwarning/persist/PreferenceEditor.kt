package org.drewhamilton.locationwarning.persist

import android.content.Context
import android.content.SharedPreferences
import androidx.content.edit
import javax.inject.Inject

class PreferenceEditor @Inject constructor(applicationContext: Context) {

  private val sharedPreferences: SharedPreferences = applicationContext.getSharedPreferences(Keys.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

  fun savePrimaryNotificationMessage(text: CharSequence) {
    sharedPreferences.edit {
      putString(Keys.PRIMARY_NOTIFICATION_TEXT, text.toString())
    }
  }
}
