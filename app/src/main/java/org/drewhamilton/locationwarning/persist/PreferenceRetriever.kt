package org.drewhamilton.locationwarning.persist

import android.content.Context
import android.content.SharedPreferences
import org.drewhamilton.locationwarning.R
import javax.inject.Inject

class PreferenceRetriever @Inject constructor(private val applicationContext: Context) {

  private val sharedPreferences: SharedPreferences = applicationContext.getSharedPreferences(Keys.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

  fun getPrimaryNotificationMessage(): String {
    val default = applicationContext.getString(R.string.default_primary_notification)
    return sharedPreferences.getString(Keys.PRIMARY_NOTIFICATION_TEXT, default)
  }
}
