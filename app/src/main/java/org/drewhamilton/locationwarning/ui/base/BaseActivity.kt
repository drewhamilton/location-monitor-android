package org.drewhamilton.locationwarning.ui.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import org.drewhamilton.locationwarning.inject.AppComponentHolder
import org.drewhamilton.locationwarning.permission.PermissionManager
import org.drewhamilton.locationwarning.wrapper.ExceptionThrower
import org.drewhamilton.locationwarning.wrapper.android.AppCompatDelegate
import timber.log.Timber
import javax.inject.Inject

abstract class BaseActivity<V : BaseView, P : BasePresenter<V>> : AppCompatActivity() {

  protected companion object {
    const val DEFAULT_ANIMATION_DURATION_MS = 250

    private const val NIGHT_MODE = android.support.v7.app.AppCompatDelegate.MODE_NIGHT_AUTO
  }

  protected lateinit var presenter: P
    private set

  private lateinit var injectedMembers: InjectedMembers

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(getLayout())
    ButterKnife.bind(this)

    injectedMembers = InjectedMembers()
    requestLocationPermissions()
    enableNightMode()

    presenter = newPresenter()
    try {
      thisAsView()

      onPreAttachPresenter()
      attachToPresenter()
    } catch (exception: ClassCastException) {
      injectedMembers.exceptionThrower throwException InvalidViewImplementationException()
    }
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter.detachView()
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    when (requestCode) {
      PermissionManager.REQUEST_CODE_LOCATION ->
        Timber.v("Received result for location permission: %s", injectedMembers.permissionManager.hasLocationPermission(this))
      else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
  }

  @LayoutRes
  protected abstract fun getLayout(): Int

  protected abstract fun newPresenter(): P

  // Empty but available for overriding; called just before presenter is initialized
  open protected fun onPreAttachPresenter() {}

  @Suppress("UNCHECKED_CAST") // Checked in onCreate()
  private fun thisAsView(): V = this as V

  private fun requestLocationPermissions() {
    if (!injectedMembers.permissionManager.hasLocationPermission(this)) {
      injectedMembers.permissionManager.requestLocationPermission(this)
    }
  }

  private fun enableNightMode() {
    if (injectedMembers.appCompatDelegate.getDefaultNightMode() != NIGHT_MODE) {
      injectedMembers.appCompatDelegate.setDefaultNightMode(NIGHT_MODE)
    }
  }

  private fun attachToPresenter() = presenter.attachView(thisAsView())

  class InjectedMembers {

    @Inject lateinit var exceptionThrower: ExceptionThrower
    @Inject lateinit var appCompatDelegate: AppCompatDelegate
    @Inject lateinit var permissionManager: PermissionManager

    init {
      AppComponentHolder.getAppComponent().inject(this)
    }
  }
}
