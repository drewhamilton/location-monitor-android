package org.drewhamilton.locationwarning.ui.base

abstract class BasePresenter<V : BaseView> {

  protected var view: V? = null
    private set

  open fun attachView(view: V) {
    this.view = view
  }

  open fun detachView() {
    this.view = null
  }
}
