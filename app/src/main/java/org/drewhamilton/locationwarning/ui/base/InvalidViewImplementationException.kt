package org.drewhamilton.locationwarning.ui.base

class InvalidViewImplementationException : RuntimeException("Subclass of BaseActivity must implement V")
