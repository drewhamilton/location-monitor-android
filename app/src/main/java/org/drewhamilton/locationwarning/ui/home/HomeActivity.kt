package org.drewhamilton.locationwarning.ui.home

import android.animation.ValueAnimator
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import butterknife.BindView
import butterknife.OnClick
import org.drewhamilton.locationwarning.R
import org.drewhamilton.locationwarning.inject.AppComponentHolder
import org.drewhamilton.locationwarning.ui.base.BaseActivity
import timber.log.Timber

class HomeActivity : BaseActivity<HomeView, HomePresenter>(), HomeView {

  @BindView(R.id.background_home)
  lateinit var backgroundView: View

  @BindView(R.id.text_view_primary_message)
  lateinit var notificationTextView: EditText

  @BindView(R.id.button_show_notification)
  lateinit var showNotificationButton: View

  @BindView(R.id.button_hide_notification)
  lateinit var hideNotificationButton: View

  @LayoutRes
  override fun getLayout() = R.layout.activity_home

  override fun newPresenter() = AppComponentHolder.getAppComponent().homePresenter()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    Timber.v("Adding text changed listener")
    notificationTextView.addTextChangedListener(NotificationTextWatcher())
  }

  //region HomeView methods
  override fun displayActiveNotificationUi() {
    Timber.v("displayActiveNotificationUi()")
    showNotificationButton.isEnabled = false
    hideNotificationButton.isEnabled = true
    displayActiveBackgroundColor()
  }

  override fun displayInactiveNotificationUi() {
    Timber.v("displayInactiveNotificationUi()")
    hideNotificationButton.isEnabled = false
    showNotificationButton.isEnabled = true
    displayNeutralBackgroundColor()
  }

  override fun displayNeutralUi() {
    Timber.v("displayNeutralUi()")
    showNotificationButton.isEnabled = true
    hideNotificationButton.isEnabled = true
    displayNeutralBackgroundColor()
  }

  override fun displayDraftText(text: CharSequence) {
    Timber.v("displayDraftText(): \"%s\"", text)
    notificationTextView.setText(text)
  }

  override fun setCanPostNotification(canPostNotification: Boolean) {
    showNotificationButton.isEnabled = canPostNotification
  }
  //endregion

  @OnClick(R.id.button_show_notification)
  fun showNotification() {
    Timber.v("Show notification button clicked")
    presenter.showNotification(notificationTextView.text.toString())
  }

  @OnClick(R.id.button_hide_notification)
  fun hideNotification() {
    Timber.v("Hide notification button clicked")
    presenter.hideNotification()
  }

  private fun displayActiveBackgroundColor() {
    Timber.v("Displaying active background color")
    changeBackgroundColor(true)
  }

  private fun displayNeutralBackgroundColor() {
    Timber.v("Displaying neutral background color")
    changeBackgroundColor(false)
  }

  private fun changeBackgroundColor(setActive: Boolean) {
    Timber.v("changeBackgroundColor() to %s", if (setActive) "active" else "inactive")
    with(ValueAnimator.ofFloat(backgroundView.alpha, if (setActive) 1f else 0f)) {
      duration = DEFAULT_ANIMATION_DURATION_MS.toLong()
      addUpdateListener { animation -> backgroundView.alpha = animation.animatedValue as Float }
      start()
    }
  }

  private inner class NotificationTextWatcher : TextWatcher {

    override fun afterTextChanged(s: Editable?) {
      // No-op
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
      // No-op
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
      Timber.v("onTextChanged(): %d characters changed", count)
      presenter.updateDraftText(s?.let { s.toString() } ?: "")
    }
  }
}
