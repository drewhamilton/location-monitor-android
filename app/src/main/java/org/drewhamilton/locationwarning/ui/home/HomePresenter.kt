package org.drewhamilton.locationwarning.ui.home

import org.drewhamilton.locationwarning.notification.PrimaryNotificationController
import org.drewhamilton.locationwarning.persist.PreferenceEditor
import org.drewhamilton.locationwarning.persist.PreferenceRetriever
import org.drewhamilton.locationwarning.ui.base.BasePresenter
import timber.log.Timber
import javax.inject.Inject

class HomePresenter @Inject constructor(
    private val notificationController: PrimaryNotificationController,
    private val preferenceEditor: PreferenceEditor,
    private val preferenceRetriever: PreferenceRetriever)
  : BasePresenter<HomeView>() {

  private var draftText: String = ""

  override fun attachView(view: HomeView) {
    super.attachView(view)

    draftText = preferenceRetriever.getPrimaryNotificationMessage()
    view.displayDraftText(draftText)

    if (notificationController.canDetermineActiveNotifications()) {
      if (notificationController.isPrimaryNotificationShowing()) {
        view.displayActiveNotificationUi()
      } else {
        view.displayInactiveNotificationUi()
      }
    } else {
      view.displayNeutralUi()
    }

    if (draftText.isEmpty()) {
      view.setCanPostNotification(false)
    }
  }

  fun updateDraftText(text: String) {
    draftText = text

    val canPostNotification =
        if (draftText.isEmpty()) {
          false
        } else {
          if (isNotificationKnownToBeShowing()) {
            draftText != preferenceRetriever.getPrimaryNotificationMessage()
          } else {
            true
          }
        }

    view?.setCanPostNotification(canPostNotification)
  }

  fun showNotification(text: String) {
    preferenceEditor.savePrimaryNotificationMessage(text)
    notificationController.postPrimaryNotification()
    Timber.v("Posted primary notification")
    view?.displayActiveNotificationUi()
  }

  fun hideNotification() {
    notificationController.hidePrimaryNotification()
    Timber.v("Hid primary notification")
    view?.displayInactiveNotificationUi()
    view?.setCanPostNotification(!draftText.isEmpty())
  }

  private fun isNotificationKnownToBeShowing() = notificationController.canDetermineActiveNotifications()
      && notificationController.isPrimaryNotificationShowing()
}
