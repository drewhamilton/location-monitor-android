package org.drewhamilton.locationwarning.ui.home

import org.drewhamilton.locationwarning.ui.base.BaseView

interface HomeView : BaseView {

  fun displayActiveNotificationUi()
  fun displayInactiveNotificationUi()
  fun displayNeutralUi()

  fun displayDraftText(text: CharSequence)

  fun setCanPostNotification(canPostNotification: Boolean)
}
