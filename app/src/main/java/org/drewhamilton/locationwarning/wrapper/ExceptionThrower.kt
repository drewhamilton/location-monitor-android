package org.drewhamilton.locationwarning.wrapper

import javax.inject.Inject

// Used for throwing exceptions from places where the exception thrown can't be easily
// tested, e.g. in an Activity's onCreate() method
class ExceptionThrower @Inject constructor() {

  // Ideally would use return type "Nothing", but that screws up the tests
  infix fun throwException(throwable: Throwable): Unit = throw throwable
}
