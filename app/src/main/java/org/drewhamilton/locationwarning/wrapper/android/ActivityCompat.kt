package org.drewhamilton.locationwarning.wrapper.android

import android.app.Activity
import android.content.Context
import android.support.annotation.IntRange
import android.support.v4.app.ActivityCompat
import javax.inject.Inject

open class ActivityCompat @Inject constructor() {

  fun checkSelfPermission(context: Context, permission: String): Int = ActivityCompat.checkSelfPermission(context, permission)

  fun requestPermissions(activity: Activity, permissions: Array<String>, @IntRange(from = 0) requestCode: Int)
      = ActivityCompat.requestPermissions(activity, permissions, requestCode)

  fun shouldShowRequestPermissionRationale(activity: Activity, permission: String): Boolean
      = ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)
}
