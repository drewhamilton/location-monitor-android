package org.drewhamilton.locationwarning.wrapper.android

import android.os.Build
import javax.inject.Inject

class AndroidVersion @Inject constructor() {

  /*
   * For inspection purposes, can't use convenience methods or constants to minimize duplication, because Android
   * Studio inspections only go down 1 level of method calls to verify that Android API calls are legal. So every
   * method in this class is pretty repetitive.
   */

  fun isAtLeastOreo(): Boolean {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
  }

  fun isAtLeastMarshmallow(): Boolean {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
  }
}
