package org.drewhamilton.locationwarning.wrapper.android

import android.support.v7.app.AppCompatDelegate
import javax.inject.Inject

open class AppCompatDelegate @Inject constructor() {

  fun getDefaultNightMode(): Int {
    return AppCompatDelegate.getDefaultNightMode()
  }

  fun setDefaultNightMode(@AppCompatDelegate.NightMode defaultNightMode: Int) {
    AppCompatDelegate.setDefaultNightMode(defaultNightMode)
  }
}
