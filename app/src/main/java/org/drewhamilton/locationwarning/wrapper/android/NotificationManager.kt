package org.drewhamilton.locationwarning.wrapper.android

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.service.notification.StatusBarNotification
import android.support.annotation.RequiresApi
import javax.inject.Inject

open class NotificationManager @Inject constructor(private val androidNotificationManager: NotificationManager) {

  fun notify(id: Int, notification: Notification) = androidNotificationManager.notify(id, notification)

  fun cancel(id: Int) = androidNotificationManager.cancel(id)

  @RequiresApi(Build.VERSION_CODES.M)
  fun activeNotifications(): Array<StatusBarNotification> = androidNotificationManager.activeNotifications

  @RequiresApi(Build.VERSION_CODES.O)
  fun createNotificationChannel(notificationChannel: NotificationChannel)
      = androidNotificationManager.createNotificationChannel(notificationChannel)
}
