package org.drewhamilton.locationwarning.wrapper.android.factory

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import javax.inject.Inject

class IntentFactory @Inject constructor() {

  fun intent(context: Context, jClass: Class<out Activity>): Intent = Intent(context, jClass)

  fun createActivityPendingIntent(context: Context, requestCode: Int, intent: Intent, flags: Int): PendingIntent
      = PendingIntent.getActivity(context, requestCode, intent, flags)
}
