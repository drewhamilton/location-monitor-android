package org.drewhamilton.locationwarning.wrapper.android.factory

import android.app.NotificationChannel
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import javax.inject.Inject

class NotificationFactory @Inject constructor(private val applicationContext: Context) {

  @RequiresApi(Build.VERSION_CODES.O)
  fun notificationChannel(id: String, name: String, importance: Int) = NotificationChannel(id, name, importance)

  @RequiresApi(Build.VERSION_CODES.O)
  fun notificationBuilder(channel: NotificationChannel) = NotificationCompat.Builder(applicationContext, channel.id)

  fun notificationBuilder() = NotificationCompat.Builder(applicationContext)

  fun bigTextStyle() = NotificationCompat.BigTextStyle()
}
