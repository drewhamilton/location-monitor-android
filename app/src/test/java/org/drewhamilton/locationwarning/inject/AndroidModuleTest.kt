package org.drewhamilton.locationwarning.inject

import android.app.NotificationManager
import android.content.Context
import com.nhaarman.mockito_kotlin.whenever
import org.amshove.kluent.`should be`
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class AndroidModuleTest {

  @Mock private lateinit var mockApplicationContext: Context

  @Mock private lateinit var mockNotificationManager: NotificationManager

  private lateinit var androidModule: AndroidModule

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)

    whenever(mockApplicationContext.getSystemService(Context.NOTIFICATION_SERVICE)).thenReturn(mockNotificationManager)

    androidModule = AndroidModule()
  }

  @Test
  fun `provideNotificationManager() returnsSystemService`() {
    val result = androidModule.provideNotificationManager(mockApplicationContext)

    result `should be` mockNotificationManager
  }
}
