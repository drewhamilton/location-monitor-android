package org.drewhamilton.locationwarning.inject

import org.amshove.kluent.`should be`
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class AppComponentHolderTest {

  @Mock private lateinit var mockAppComponent: AppComponent

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
  }

  @Test
  fun `setAppComponent() setsAppComponent`() {
    AppComponentHolder.setAppComponent(mockAppComponent)

    val result = AppComponentHolder.getAppComponent()

    result `should be` mockAppComponent
  }
}
