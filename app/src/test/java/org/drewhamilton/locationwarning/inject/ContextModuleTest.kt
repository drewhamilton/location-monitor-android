package org.drewhamilton.locationwarning.inject

import android.content.Context
import org.amshove.kluent.`should be`
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ContextModuleTest {

  @Mock private lateinit var mockApplicationContext: Context

  @InjectMocks private lateinit var contextModule: ContextModule

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
  }

  @Test
  fun `provideApplicationContext() returnsApplicationContext`() {
    val result = contextModule.provideApplicationContext()

    result `should be` mockApplicationContext
  }
}
