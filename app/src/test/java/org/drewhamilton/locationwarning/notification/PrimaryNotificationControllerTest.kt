package org.drewhamilton.locationwarning.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.service.notification.StatusBarNotification
import android.support.v4.app.NotificationCompat
import com.nhaarman.mockito_kotlin.*
import org.amshove.kluent.`should be`
import org.amshove.kluent.`should equal`
import org.drewhamilton.locationwarning.R
import org.drewhamilton.locationwarning.persist.PreferenceRetriever
import org.drewhamilton.locationwarning.ui.home.HomeActivity
import org.drewhamilton.locationwarning.wrapper.android.AndroidVersion
import org.drewhamilton.locationwarning.wrapper.android.NotificationManager
import org.drewhamilton.locationwarning.wrapper.android.factory.IntentFactory
import org.drewhamilton.locationwarning.wrapper.android.factory.NotificationFactory
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PrimaryNotificationControllerTest {

  private companion object {
    const val CHANNEL_ID = "Test channel ID"
    const val CHANNEL_NAME = "Test channel name"
    const val CHANNEL_DESCRIPTION = "Test channel description"

    const val INTENT_FLAGS = Intent.FLAG_ACTIVITY_CLEAR_TOP

    const val WARNING = "Warning"
    const val DESCRIPTION = "Notification description"
    const val COLOR = 99
    const val NOTIFICATION_ID = 1
    const val NOTIFICATION_DURATION_MS = 1000
  }

  @Mock private lateinit var mockApplicationContext: Context
  @Mock private lateinit var mockAndroidVersion: AndroidVersion
  @Mock private lateinit var mockNotificationFactory: NotificationFactory
  @Mock private lateinit var mockNotificationManager: NotificationManager
  @Mock private lateinit var mockPreferenceRetriever: PreferenceRetriever
  @Mock private lateinit var mockIntentFactory: IntentFactory

  @Mock private lateinit var mockNotificationChannel: NotificationChannel
  @Mock private lateinit var mockNotificationBuilder: NotificationCompat.Builder
  @Mock private lateinit var mockNotification: Notification
  @Mock private lateinit var mockNotificationStyle: NotificationCompat.BigTextStyle

  @Mock private lateinit var mockIntent: Intent
  @Mock private lateinit var mockPendingIntent: PendingIntent

  @InjectMocks private lateinit var primaryNotificationController: PrimaryNotificationController

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)

    val mockResources: Resources = mock()
    whenever(mockResources.getColor(R.color.primaryDark)).thenReturn(COLOR)

    whenever(mockApplicationContext.resources).thenReturn(mockResources)
    whenever(mockApplicationContext.getString(R.string.default_notification_channel_id)).thenReturn(CHANNEL_ID)
    whenever(mockApplicationContext.getString(R.string.default_notification_channel_name)).thenReturn(CHANNEL_NAME)
    whenever(mockApplicationContext.getString(R.string.default_notification_channel_description)).thenReturn(CHANNEL_DESCRIPTION)
    whenever(mockApplicationContext.getString(R.string.warning)).thenReturn(WARNING)

    whenever(mockNotificationBuilder.build()).thenReturn(mockNotification)

    whenever(mockNotificationFactory.notificationChannel(any(), any(), any())).thenAnswer { invocation ->
      whenever(mockNotificationChannel.id).thenReturn(invocation.getArgument(0))
      whenever(mockNotificationChannel.name).thenReturn(invocation.getArgument(1))
      whenever(mockNotificationChannel.importance).thenReturn(invocation.getArgument(2))
      mockNotificationChannel
    }
    whenever(mockNotificationFactory.notificationBuilder(mockNotificationChannel)).thenReturn(mockNotificationBuilder)
    whenever(mockNotificationFactory.notificationBuilder()).thenReturn(mockNotificationBuilder)
    whenever(mockNotificationFactory.bigTextStyle()).thenReturn(mockNotificationStyle)

    whenever(mockIntentFactory.intent(mockApplicationContext, HomeActivity::class.java)).thenReturn(mockIntent)
    whenever(mockIntentFactory.createActivityPendingIntent(mockApplicationContext, 0, mockIntent, INTENT_FLAGS)).thenReturn(mockPendingIntent)

    whenever(mockPreferenceRetriever.getPrimaryNotificationMessage()).thenReturn(DESCRIPTION)
  }

  //region postPrimaryNotification()
  @Test
  fun `postPrimaryNotification() onOreo postsNotificationChannelAndNotification`() {
    whenever(mockAndroidVersion.isAtLeastOreo()).thenReturn(true)

    primaryNotificationController.postPrimaryNotification()

    mockNotificationChannel.id `should equal` CHANNEL_ID
    mockNotificationChannel.name `should equal` CHANNEL_NAME
    mockNotificationChannel.importance `should be` android.app.NotificationManager.IMPORTANCE_HIGH
    verify(mockNotificationChannel).description = CHANNEL_DESCRIPTION
    verify(mockNotificationChannel).lightColor = Color.RED
    verify(mockNotificationChannel).enableLights(true)

    verify(mockNotificationManager).createNotificationChannel(mockNotificationChannel)

    verify(mockNotificationFactory).notificationBuilder(mockNotificationChannel)
    verify(mockNotificationFactory, never()).notificationBuilder()

    verify(mockNotificationStyle).bigText(DESCRIPTION)

    verifyPostPrimaryNotification()
  }

  @Test
  fun `postPrimaryNotification() notOnOreo postsNotificationOnly`() {
    whenever(mockAndroidVersion.isAtLeastOreo()).thenReturn(false)

    primaryNotificationController.postPrimaryNotification()

    verifyNoMoreInteractions(mockNotificationChannel)
    verify(mockNotificationManager, never()).createNotificationChannel(mockNotificationChannel)
    verify(mockNotificationFactory, never()).notificationBuilder(mockNotificationChannel)
    verify(mockNotificationFactory).notificationBuilder()

    verifyPostPrimaryNotification()
  }

  private fun verifyPostPrimaryNotification() {
    verify(mockIntent).flags = INTENT_FLAGS

    verify(mockNotificationStyle).bigText(DESCRIPTION)

    verify(mockNotificationBuilder).setContentTitle(WARNING)
    verify(mockNotificationBuilder).setContentText(DESCRIPTION)
    verify(mockNotificationBuilder).setSmallIcon(R.drawable.ic_warning_white_24dp)
    verify(mockNotificationBuilder).setContentIntent(mockPendingIntent)
    verify(mockNotificationBuilder).setStyle(mockNotificationStyle)
    verify(mockNotificationBuilder).setOngoing(true)
    verify(mockNotificationBuilder).color = COLOR
    verify(mockNotificationBuilder).setLights(Color.RED, NOTIFICATION_DURATION_MS, NOTIFICATION_DURATION_MS)
    verify(mockNotificationBuilder).setSound(null)

    verify(mockNotificationManager).notify(NOTIFICATION_ID, mockNotification)
  }
  //endregion

  //region hidePrimaryNotification()
  @Test
  fun `hidePrimaryNotification() hidesNotification`() {
    primaryNotificationController.hidePrimaryNotification()

    verify(mockNotificationManager).cancel(NOTIFICATION_ID)
  }
  //endregion

  //region canDetermineActiveNotifications()
  @Test
  fun `canDetermineActiveNotifications() isMarshmallow returnsTrue`() {
    whenever(mockAndroidVersion.isAtLeastMarshmallow()).thenReturn(true)

    val result = primaryNotificationController.canDetermineActiveNotifications()

    result `should be` true
  }

  @Test
  fun `canDetermineActiveNotifications() isNotMarshmallow returnsFalse`() {
    whenever(mockAndroidVersion.isAtLeastMarshmallow()).thenReturn(false)

    val result = primaryNotificationController.canDetermineActiveNotifications()

    result `should be` false
  }
  //endregion

  //region isPrimaryNotificationShowing()
  @Test
  fun `isPrimaryNotificationShowing() cannotFindId returnsFalse`() {
    testIsPrimaryNotificationShowing({ i -> 2*NOTIFICATION_ID + i }, false)
  }

  @Test
  fun `isPrimaryNotificationShowing() findsId returnsTrue`() {
    testIsPrimaryNotificationShowing({ i -> NOTIFICATION_ID + i }, true)
  }

  private fun testIsPrimaryNotificationShowing(calculateNotificationId: (Int) -> Int, expectedResult: Boolean) {
    val notificationCount = 4
    val mockStatusBarNotifications: Array<StatusBarNotification> = Array(notificationCount, { i ->
      val mockStatusBarNotification: StatusBarNotification = mock()
      whenever(mockStatusBarNotification.id).thenReturn(calculateNotificationId(i))
      mockStatusBarNotification
    })
    whenever(mockNotificationManager.activeNotifications()).thenReturn(mockStatusBarNotifications)

    val result = primaryNotificationController.isPrimaryNotificationShowing()

    result `should equal` expectedResult
  }
  //endregion
}
