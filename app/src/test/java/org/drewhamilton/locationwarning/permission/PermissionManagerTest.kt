package org.drewhamilton.locationwarning.permission

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.amshove.kluent.`should be`
import org.drewhamilton.locationwarning.wrapper.android.ActivityCompat
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PermissionManagerTest {

  @Mock private lateinit var mockActivityCompat: ActivityCompat

  @Mock private lateinit var mockContext: Context
  @Mock private lateinit var mockActivity: Activity

  @InjectMocks private lateinit var permissionManager: PermissionManager

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
  }

  //region companion object
  @Test
  fun `REQUEST_CODE_LOCATION is 10`() {
    PermissionManager.REQUEST_CODE_LOCATION `should be` 10
  }
  //endregion

  //region hasLocationPermission()
  @Test
  fun `hasLocationPermission() granted returnsTrue`() {
    whenever(mockActivityCompat.checkSelfPermission(mockContext, Manifest.permission.ACCESS_FINE_LOCATION))
        .thenReturn(PackageManager.PERMISSION_GRANTED)

    val result = permissionManager.hasLocationPermission(mockContext)

    result `should be` true
  }

  @Test
  fun `hasLocationPermission() notGranted returnsFalse`() {
    whenever(mockActivityCompat.checkSelfPermission(mockContext, Manifest.permission.ACCESS_FINE_LOCATION))
        .thenReturn(PackageManager.PERMISSION_DENIED)

    val result = permissionManager.hasLocationPermission(mockContext)

    result `should be` false
  }
  //endregion

  //region shouldDisplayLocationPermissionRationale()
  @Test
  fun `shouldDisplayLocationPermissionRationale() activityCompatReturnsTrue returnsTrue`() {
    testShouldDisplayLocationPermissionRationale(true)
  }

  @Test
  fun `shouldDisplayLocationPermissionRationale() activityCompatReturnsFalse returnsFalse`() {
    testShouldDisplayLocationPermissionRationale(false)
  }

  private fun testShouldDisplayLocationPermissionRationale(expectedResult: Boolean) {
    whenever(mockActivityCompat.shouldShowRequestPermissionRationale(mockActivity, Manifest.permission.ACCESS_FINE_LOCATION))
        .thenReturn(expectedResult)

    val result = permissionManager.shouldDisplayLocationPermissionRationale(mockActivity)

    result `should be` expectedResult
  }
  //endregion

  //region requestLocationPermission()
  @Test
  fun `requestLocationPermission() makesProperRequest`() {
    permissionManager.requestLocationPermission(mockActivity)

    verify(mockActivityCompat).requestPermissions(mockActivity, Array(1, {Manifest.permission.ACCESS_FINE_LOCATION}),
        PermissionManager.REQUEST_CODE_LOCATION)
  }
  //endregion
}
