package org.drewhamilton.locationwarning.persist

import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test

class KeysTest {

  // If SHARED_PREFERENCES_NAME is changed, all previous users will lose all of their saved preferences
  @Test
  fun `SHARED_PREFERENCES_NAME valueIsExpected`() {
    Keys.SHARED_PREFERENCES_NAME shouldBeEqualTo "org.drewhamilton.locationwarning\$preferences_primary"
  }

  // If PRIMARY_NOTIFICATION_TEXT is changed, all previous users will lose their saved primary notification text preference
  @Test
  fun `PRIMARY_NOTIFICATION_TEXT valueIsExpected`() {
    Keys.PRIMARY_NOTIFICATION_TEXT shouldBeEqualTo "preferences_primary\$primaryNotificationText"
  }
}
