package org.drewhamilton.locationwarning.persist

import android.content.Context
import android.content.SharedPreferences
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.drewhamilton.locationwarning.R
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PreferenceEditorTest {

  private companion object {
    const val DEFAULT_TEST_STRING = "Default test string"
  }

  @Mock private lateinit var mockApplicationContext: Context

  @Mock private lateinit var mockSharedPreferences: SharedPreferences
  @Mock private lateinit var mockEditor: SharedPreferences.Editor

  private lateinit var preferenceEditor: PreferenceEditor

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)

    whenever(mockApplicationContext.getSharedPreferences(Keys.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)).thenReturn(mockSharedPreferences)
    whenever(mockApplicationContext.getString(R.string.default_primary_notification)).thenReturn(DEFAULT_TEST_STRING)

    whenever(mockSharedPreferences.edit()).thenReturn(mockEditor)

    whenever(mockEditor.putString(anyString(), anyString())).thenReturn(mockEditor)

    preferenceEditor = PreferenceEditor(mockApplicationContext)
  }

  @Test
  fun `savePrimaryNotificationMessage() savesValueToSharedPreferences`() {
    val testString = "Test string"

    preferenceEditor.savePrimaryNotificationMessage(testString)

    verify(mockEditor).putString(Keys.PRIMARY_NOTIFICATION_TEXT, testString)
    verify(mockEditor).apply()
  }
}
