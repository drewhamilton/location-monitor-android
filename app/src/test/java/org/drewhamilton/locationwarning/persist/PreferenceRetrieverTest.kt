package org.drewhamilton.locationwarning.persist

import android.content.Context
import android.content.SharedPreferences
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.amshove.kluent.`should equal`
import org.drewhamilton.locationwarning.R
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.Mockito.anyString
import org.mockito.MockitoAnnotations

class PreferenceRetrieverTest {

  private companion object {
    const val DEFAULT_TEST_STRING = "Default test string"
  }

  @Mock private lateinit var mockApplicationContext: Context

  @Mock private lateinit var mockSharedPreferences: SharedPreferences

  private lateinit var preferenceRetriever: PreferenceRetriever

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)

    whenever(mockApplicationContext.getSharedPreferences(Keys.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)).thenReturn(mockSharedPreferences)
    whenever(mockApplicationContext.getString(R.string.default_primary_notification)).thenReturn(DEFAULT_TEST_STRING)

    preferenceRetriever = PreferenceRetriever(mockApplicationContext)
  }

  @Test
  fun `getPrimaryNotificationMessage() returnsExpectedValue`() {
    val testString = "Test string"
    whenever(mockSharedPreferences.getString(eq(Keys.PRIMARY_NOTIFICATION_TEXT), anyString())).thenReturn(testString)

    val result = preferenceRetriever.getPrimaryNotificationMessage()

    result `should equal` testString
  }

  @Test
  fun `getPrimaryNotificationMessage() usesCorrectDefault`() {
    whenever(mockSharedPreferences.getString(eq(Keys.PRIMARY_NOTIFICATION_TEXT), anyString())).thenReturn(DEFAULT_TEST_STRING)

    val result = preferenceRetriever.getPrimaryNotificationMessage()

    verify(mockSharedPreferences).getString(Keys.PRIMARY_NOTIFICATION_TEXT, DEFAULT_TEST_STRING)
    result `should equal` DEFAULT_TEST_STRING
  }
}
