package org.drewhamilton.locationwarning.ui.base

import org.amshove.kluent.`should be`
import org.amshove.kluent.`should not be`
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BasePresenterTest {

  @Mock private lateinit var mockView: BaseView

  private val presenter = object : BasePresenter<BaseView>() {
    fun view() = view
  }

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
    mockView `should not be` null
  }

  @Test
  fun `init withNullView`() {
    presenter.view() `should be` null
  }

  @Test
  fun `attachView() attachesView`() {
    presenter.attachView(mockView)

    presenter.view() `should be` mockView
  }

  @Test
  fun `detachView() setsViewToNull`() {
    presenter.attachView(mockView)
    presenter.detachView()

    presenter.view() `should be` null
  }
}
