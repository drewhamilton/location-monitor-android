package org.drewhamilton.locationwarning.ui.base

import org.amshove.kluent.`should equal`
import org.amshove.kluent.`should not be`
import org.junit.Test

class InvalidViewImplementationExceptionTest {

  @Test
  fun `constructor hasExpectedMessage`() {
    val exception = InvalidViewImplementationException()
    exception.message `should not be` null
    exception.message `should equal` "Subclass of BaseActivity must implement V"
  }
}
