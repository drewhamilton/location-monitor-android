package org.drewhamilton.locationwarning.ui.home

import com.nhaarman.mockito_kotlin.*
import org.drewhamilton.locationwarning.notification.PrimaryNotificationController
import org.drewhamilton.locationwarning.persist.PreferenceEditor
import org.drewhamilton.locationwarning.persist.PreferenceRetriever
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class HomePresenterTest {

  private companion object {
    const val FAKE_PRIMARY_MESSAGE = "Primary message"

    enum class DisplayType {
      ACTIVE, INACTIVE, NEUTRAL
    }
  }

  @Mock private lateinit var mockPrimaryNotificationController: PrimaryNotificationController
  @Mock private lateinit var mockPreferenceEditor: PreferenceEditor
  @Mock private lateinit var mockPreferenceRetriever: PreferenceRetriever

  @Mock private lateinit var mockView: HomeView

  @InjectMocks private lateinit var presenter: HomePresenter

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)

    whenever(mockPreferenceRetriever.getPrimaryNotificationMessage()).thenReturn(FAKE_PRIMARY_MESSAGE)
  }

  //region attachView()
  @Test
  fun `attachView() primaryNotificationIsShowing displaysActiveNotificationUi`() {
    testAttachView(true, true, FAKE_PRIMARY_MESSAGE, DisplayType.ACTIVE, false)
  }

  @Test
  fun `attachView() primaryNotificationIsNotShowing displaysInactiveNotificationUi`() {
    testAttachView(true, false, FAKE_PRIMARY_MESSAGE, DisplayType.INACTIVE, false)
  }

  @Test
  fun `attachView() primaryNotificationCannotBeDetermined displaysNeutralUi`() {
    testAttachView(false, false, FAKE_PRIMARY_MESSAGE, DisplayType.NEUTRAL, false)
  }

  @Test
  fun `attachView() draftTextIsEmpty doesNotAllowNotificationPost`() {
    testAttachView(true, false, "", DisplayType.INACTIVE, true)
  }

  private fun testAttachView(canDetermineActiveNotifications: Boolean, isPrimaryNotificationShowing: Boolean, savedMessage: String,
      expectedDisplayType: DisplayType, expectedDisallowsPostNotification: Boolean) {

    whenever(mockPrimaryNotificationController.canDetermineActiveNotifications()).thenReturn(canDetermineActiveNotifications)
    whenever(mockPrimaryNotificationController.isPrimaryNotificationShowing()).thenReturn(isPrimaryNotificationShowing)

    whenever(mockPreferenceRetriever.getPrimaryNotificationMessage()).thenReturn(savedMessage)

    presenter.attachView(mockView)

    verify(mockView).displayDraftText(savedMessage)
    when (expectedDisplayType) {
      DisplayType.ACTIVE -> verify(mockView).displayActiveNotificationUi()
      DisplayType.INACTIVE -> verify(mockView).displayInactiveNotificationUi()
      DisplayType.NEUTRAL -> verify(mockView).displayNeutralUi()
    }

    if (expectedDisallowsPostNotification) {
      verify(mockView).setCanPostNotification(false)
    }
  }
  //endregion

  //region updateDraftText()
  @Test
  fun `updateDraftText() draftTextIsEmpty cannotDisplayNotification`() {
    testUpdateDraftText(false, false, "Not empty saved message", "", false)
  }

  @Test
  fun `updateDraftText() sameTextIsAlreadyShowing cannotDisplayNotification`() {
    testUpdateDraftText(true, true, FAKE_PRIMARY_MESSAGE, FAKE_PRIMARY_MESSAGE, false)
  }

  @Test
  fun `updateDraftText() differentTextIsShowing canDisplayNotification`() {
    testUpdateDraftText(true, true, FAKE_PRIMARY_MESSAGE, FAKE_PRIMARY_MESSAGE + "!", true)
  }

  @Test
  fun `updateDraftText() sameTextIsSavedButNotShowing canDisplayNotification`() {
    testUpdateDraftText(true, false, FAKE_PRIMARY_MESSAGE, FAKE_PRIMARY_MESSAGE, true)
  }

  @Test
  fun `updateDraftText() activeNotificationCannotBeDetermined canDisplayNotification`() {
    testUpdateDraftText(false, false, FAKE_PRIMARY_MESSAGE, FAKE_PRIMARY_MESSAGE, true)
  }

  private fun testUpdateDraftText(
      canDetermineActiveNotifications: Boolean, isPrimaryNotificationShowing: Boolean, savedMessage: String,
      draftText: String, expectedCanPostNotification: Boolean) {

    whenever(mockPrimaryNotificationController.canDetermineActiveNotifications()).thenReturn(canDetermineActiveNotifications)
    whenever(mockPrimaryNotificationController.isPrimaryNotificationShowing()).thenReturn(isPrimaryNotificationShowing)

    whenever(mockPreferenceRetriever.getPrimaryNotificationMessage()).thenReturn(savedMessage)

    presenter.attachView(mockView)
    reset(mockView)
    presenter.updateDraftText(draftText)

    verify(mockView).setCanPostNotification(expectedCanPostNotification)
  }
  //endregion

  //region showNotification()
  @Test
  fun `showNotification() withViewAttached displaysNotificationAndActiveUi`() {
    testShowNotification(true)
  }

  @Test
  fun `showNotification() withoutViewAttached displaysNotification`() {
    testShowNotification(false)
  }

  private fun testShowNotification(withAttachedView: Boolean) {
    if (withAttachedView) presenter.attachView(mockView)
    reset(mockView)

    presenter.showNotification(FAKE_PRIMARY_MESSAGE)

    verify(mockPreferenceEditor).savePrimaryNotificationMessage(FAKE_PRIMARY_MESSAGE)
    verify(mockPrimaryNotificationController).postPrimaryNotification()

    val expectedViewInteractionCount = if (withAttachedView) times(1) else never()
    verify(mockView, expectedViewInteractionCount).displayActiveNotificationUi()
  }
  //endregion

  //region hideNotification()
  @Test
  fun `hideNotification() withoutViewAttached hidesNotification`() {
    testHideNotification(false, FAKE_PRIMARY_MESSAGE, true)
  }

  @Test
  fun `hideNotification() withNonEmptyDraftText hidesNotificationAndUpdatesUi`() {
    testHideNotification(true, FAKE_PRIMARY_MESSAGE, true)
  }

  @Test
  fun `hideNotification() withEmptyDraftText hidesNotificationAndDisallowsPostingNotification`() {
    testHideNotification(true, "", false)
  }

  private fun testHideNotification(withAttachedView: Boolean, draftText: String, expectedCanPostNotification: Boolean) {
    if (withAttachedView) presenter.attachView(mockView)
    presenter.updateDraftText(draftText)
    reset(mockView)

    presenter.hideNotification()

    verify(mockPrimaryNotificationController).hidePrimaryNotification()

    val viewInteractionCount = if (withAttachedView) times(1) else never()
    verify(mockView, viewInteractionCount).displayInactiveNotificationUi()
    verify(mockView, viewInteractionCount).setCanPostNotification(expectedCanPostNotification)
  }
  //endregion
}
