package org.drewhamilton.locationwarning.wrapper

import org.drewhamilton.locationwarning.ui.base.InvalidViewImplementationException
import org.junit.Before
import org.junit.Test

class ExceptionThrowerTest {

  private lateinit var exceptionThrower: ExceptionThrower

  @Before
  fun setUp() {
    exceptionThrower = ExceptionThrower()
  }

  @Test(expected = InvalidViewImplementationException::class)
  fun `throwException() throwsException`() {
    exceptionThrower throwException InvalidViewImplementationException()
  }
}
