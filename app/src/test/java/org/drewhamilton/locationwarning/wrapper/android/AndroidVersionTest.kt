package org.drewhamilton.locationwarning.wrapper.android

import android.os.Build
import org.amshove.kluent.`should be`
import org.junit.Before
import org.junit.Test

class AndroidVersionTest {

  private lateinit var androidVersion: AndroidVersion

  @Before
  fun setUp() {
    androidVersion = AndroidVersion()
  }

  @Test
  fun `isAtLeastOreo() returnsCorrectValue`() {
    testIsAtLeast(Build.VERSION_CODES.O, { androidVersion.isAtLeastOreo() })
  }

  @Test
  fun `isAtLeastMarshmallow() returnsCorrectValue`() {
    testIsAtLeast(Build.VERSION_CODES.M, { androidVersion.isAtLeastMarshmallow() })
  }

  private fun testIsAtLeast(version: Int, methodUnderTest: () -> Boolean) {
    val actual = Build.VERSION.SDK_INT >= version
    val result = methodUnderTest.invoke()

    result `should be` actual
  }
}
