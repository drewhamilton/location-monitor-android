package org.drewhamilton.locationwarning.wrapper.android

import org.amshove.kluent.`should be`
import org.amshove.kluent.`should not be`
import org.junit.Before
import org.junit.Test

class AppCompatDelegateTest {

  private lateinit var appCompatDelegateWrapper: AppCompatDelegate

  @Before
  fun setUp() {
    appCompatDelegateWrapper = AppCompatDelegate()
  }

  @Test
  fun `getDefaultNightMode() returnsAndroidDefaultNightMode`() {
    val expectedResult = android.support.v7.app.AppCompatDelegate.MODE_NIGHT_YES

    val preliminaryResult = appCompatDelegateWrapper.getDefaultNightMode()
    preliminaryResult `should not be` expectedResult

    android.support.v7.app.AppCompatDelegate.setDefaultNightMode(expectedResult)

    val result = appCompatDelegateWrapper.getDefaultNightMode()

    result `should be` expectedResult
  }

  @Test
  fun `setDefaultNightMode() setsAndroidDefaultNightMode`() {
    val setValue = android.support.v7.app.AppCompatDelegate.MODE_NIGHT_NO

    val preliminaryResult = android.support.v7.app.AppCompatDelegate.getDefaultNightMode()
    preliminaryResult `should not be` setValue

    appCompatDelegateWrapper.setDefaultNightMode(setValue)

    val result = android.support.v7.app.AppCompatDelegate.getDefaultNightMode()

    result `should be` setValue
  }
}
