package org.drewhamilton.locationwarning.wrapper.android

import android.app.Notification
import android.app.NotificationChannel
import android.service.notification.StatusBarNotification
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.amshove.kluent.`should be`
import org.amshove.kluent.`should contain`
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class NotificationManagerTest {

  @Mock private lateinit var mockAndroidNotificationManager: android.app.NotificationManager

  @InjectMocks private lateinit var notificationManager: NotificationManager

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
  }

  @Test
  fun `notify() callsAndroidNotify`() {
    val id = 3298
    val mockNotification: Notification = mock()

    notificationManager.notify(id, mockNotification)

    verify(mockAndroidNotificationManager).notify(id, mockNotification)
  }

  @Test
  fun `cancel() callsAndroidCancel`() {
    val id = 983

    notificationManager.cancel(id)

    verify(mockAndroidNotificationManager).cancel(id)
  }

  @Test
  fun `activeNotifications() getsAndroidActiveNotifications`() {
    val mockNotification: StatusBarNotification = mock()
    val fakeNotificationArray: Array<StatusBarNotification> = arrayOf(mockNotification)
    whenever(mockAndroidNotificationManager.activeNotifications).thenReturn(fakeNotificationArray)

    val result = notificationManager.activeNotifications()
    result.size `should be` 1
    result `should contain` mockNotification
    result `should be` fakeNotificationArray
  }

  @Test
  fun `createNotificationChannel() callsAndroidCreateNotificationChannel`() {
    val mockNotificationChannel: NotificationChannel = mock()

    notificationManager.createNotificationChannel(mockNotificationChannel)

    verify(mockAndroidNotificationManager).createNotificationChannel(mockNotificationChannel)
  }
}
